import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ReminderType} from '../../../Types/ReminderType';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReminderService {


  constructor(private http: HttpClient) {
  }

  public getReminders(): Observable<ReminderType[]> {
    return this.http.get<ReminderType[]>('http://localhost:8080/reminder');
  }

  public getUpcomingReminders(): Observable<ReminderType[]> {
    return this.http.get<ReminderType[]>('http://localhost:8080/reminder/getUpcoming');
  }

  public getTodayReminders(): Observable<ReminderType[]> {
    return this.http.get<ReminderType[]>('http://localhost:8080/reminder/getToday');
  }

  public getPassedReminders(): Observable<ReminderType[]> {
    return this.http.get<ReminderType[]>('http://localhost:8080/reminder/getPassed');
  }

  public createReminder(clientId: string, date: Date, message: string, status: boolean): void {
    this.http.post('http://localhost:8080/reminder/client/' + clientId, {date, message, status}).subscribe();
  }

  public updateReminder(reminder: ReminderType, clientId: string): void {
    this.http.put('http://localhost:8080/reminder/client/' + clientId, reminder).subscribe();
  }
}

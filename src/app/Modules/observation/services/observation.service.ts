import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ObservationType} from '../../../Types/ObservationType';

@Injectable({
  providedIn: 'root'
})
export class ObservationService {

  constructor(private http: HttpClient) {
  }

  public getObservations(clientId: string): Observable<ObservationType[]> {
    return this.http.get<ObservationType[]>('http://localhost:8080/observation/client/' + clientId);
  }

  public createWithClient(clientId: string, observation: ObservationType): void {
    this.http.post('http://localhost:8080/observation/' + clientId, observation).subscribe();
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ClientType} from '../../../Types/ClientType';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  public getClients(username: string): Observable<ClientType[]> {
    const params = new HttpParams().set('username', username);
    return this.http.get<ClientType[]>('http://localhost:8080/client', {params});
  }

  public getById(id: string): Observable<ClientType> {

    return this.http.get<ClientType>('http://localhost:8080/client/' + id);
  }

  public createClient(username: string, name: string, cnp: string,
                      phoneNumber: string, emailAddress: string, gdprStatus: boolean, leadStatus: boolean): void {
    console.log(name);
    const params = new HttpParams().set('username', username);
    this.http.post('http://localhost:8080/client', {
      name,
      cnp,
      phoneNumber,
      emailAddress,
      gdprStatus,
      leadStatus
    }, {params}).subscribe();
  }

  public updateClient(username: string, client: ClientType): void {
    const params = new HttpParams().set('username', username);
    this.http.put('http://localhost:8080/client', client, {params}).subscribe();
  }


  public find(username: string, name: string, cnp: string, searchNumber: string): Observable<ClientType[]> {
    const params = new HttpParams().set('username', username).set('name', name).set('cnp', cnp).set('number', searchNumber);
    return this.http.get<ClientType[]>('http://localhost:8080/client', {params});
  }

  public checkCnp(cnp: string): Observable<boolean> {
    const params = new HttpParams().set('cnp', cnp);
    return this.http.get<boolean>('http://localhost:8080/client/checkCnp', {params});
  }
}
